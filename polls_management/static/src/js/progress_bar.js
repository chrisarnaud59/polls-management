/** @odoo-module **/
import { ProgressBarField } from "@web/views/fields/progress_bar/progress_bar_field";
import { patch } from "@web/core/utils/patch";
import { useEffect } from "@odoo/owl";
patch(ProgressBarField.prototype,{
   setup(v){
       var value = this.props.record.data.probability;
       super.setup(v)
       useEffect(() => this._render_value());
   },
   _render_value: function (v) {
        var value = this.props.record.data.probability;
        var max_value = this.max_value;
        if(!isNaN(v)) {
            if(this.edit_max_value) {
                max_value = v;
            } else {
                value = v;
            }
        }
       value = value || 0;
       max_value = max_value || 0;
       var widthComplete;

           widthComplete = value;

       $('.o_progress').toggleClass('o_progress_overflow', value > max_value)
           .attr('aria-valuemin', '0')
           .attr('aria-valuemax', max_value)
           .attr('aria-valuenow', value);
        $('.bg-primary').toggleClass('o_progress_red',widthComplete>0 && widthComplete<=20).css('width', widthComplete + '%');
        $('.bg-primary').toggleClass('o_progress_orange',widthComplete>20 && widthComplete<=30).css('width', widthComplete + '%');
        $('.bg-primary').toggleClass('o_progress_yellow',widthComplete>30 && widthComplete<=80).css('width', widthComplete + '%');
        $('.bg-primary').toggleClass('o_progress_green',widthComplete>80 && widthComplete<=100).css('width', widthComplete + '%');
   console.log(widthComplete)

   },

})