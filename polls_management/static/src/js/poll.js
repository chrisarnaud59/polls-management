function editDivJobCreate() {
    if (document.getElementById('first_page')) {

        if (document.getElementById('first_page').style.display == 'none') {
            document.getElementById('first_page').style.display = 'block';
            document.getElementById('second_page').style.display = 'none';
        } else {
            document.getElementById('first_page').style.display = 'none';
            document.getElementById('second_page').style.display = 'block';
        }
    }
}

function editreadonly() {
    if (document.getElementById('first_page_signup')) {

        if (document.getElementById('first_page_signup').style.display == 'none') {
            document.getElementById('first_page_signup').style.display = 'block';
            document.getElementById('second_page_signup').style.display = 'none';
        } else {
            document.getElementById('first_page_signup').style.display = 'none';
            document.getElementById('second_page_signup').style.display = 'block';
        }
    }
}

$(document).ready(function () {
    $('.poll_validate').on('click', function () {
        var btn = $(this);
        // Disable btn
        // Enable loader
        $('.btn-loader').addClass('loading');

        setTimeout(function () {
            $.ajax({
                    url: '/poll/manage',
                    data: {
                        'poller_id': btn.data('poller-id'),
                        'candidate_id': btn.data('candidate-id'),
                        'category_id': btn.data('category-id'),
                        'event_id': btn.data('event-id'),
                    },

                    type: "post",
                    dataType: 'json',
                }
            );
            document.getElementById("message_o").innerHTML = '<div class="alert alert-info mb-0 col-lg-12" role="alert"><i class="fa fa-check" aria-hidden="true"></i> Vote enregistré: Merci d\'avoir participé à l\'Evénement<div>'
            //  $(".alert-success").slideToggle("slow").delay(2000).slideToggle("slow")
            $(".poll_validate").hide();
            $('.btn-loader').removeClass('loading');
        }, 100);

    })
});

$(document).ready(function () {
    $('.poll_detail_validate').on('click', function () {
        var btn = $(this);
        // Disable btn
        // Enable loader
        $('.btn-loader').addClass('loading');

        setTimeout(function () {
            $.ajax({
                    url: '/poll/details/manage',
                    data: {
                        'poller_id': btn.data('poller-id'),
                        'nominated_id': btn.data('nominated-id'),
                        'category_id': btn.data('category-id'),
                        'event_id': btn.data('event-id')
                    },

                    type: "post",
                    dataType: 'json',
                }
            );
            document.getElementById("message_o").innerHTML = '<div class="alert alert-info mb-0 col-lg-12" role="alert"><i class="fa fa-check" aria-hidden="true"></i> Vote enregistré: Merci d\'avoir participé à l\'Evénement<div>'
            $(".poll_detail_validate").hide();
            $('.btn-loader').removeClass('loading');
        }, 100);

    })
});


$(document).ready(function () {
    $('.poll_validate_redirect').on('click', function () {
        var btn = $(this);
        // Disable btn
        // Enable loader
        $('.btn-loader').addClass('loading');
        document.getElementById("message_o_e").innerHTML = '<div class="alert alert-info mb-0 col-lg-12" role="alert">Veuillez vous inscrire ou vous connecter pour participer à l\'évènement<div>'


    })
});

$(document).ready(function () {
    $('.poll_jury_validate').on('click', function () {
        var btn = $(this);
        // Disable btn
        // Enable loader
        $('.btn-loader').addClass('loading');
        $(".post_poll_jury").submit(_=> document.getElementById("message_o").innerHTML = '<div class="alert alert-info mb-0 col-lg-12" role="alert"><i class="fa fa-check" aria-hidden="true"></i>Vote enregistré<div>');



    })
});

$(document).ready(function () {
    $('.poll_detail_validate_redirect').on('click', function () {
        var btn = $(this);
        // Disable btn
        // Enable loader
        $('.btn-loader').addClass('loading');
        document.getElementById("message_o_e").innerHTML = '<div class="alert alert-info mb-0 col-lg-12" role="alert">Veuillez vous inscrire ou vous connecter pour participer à l\'évènement<div>'


    })
});


$(document).ready(function () {
    $('.poll_jury_validate').on('click', function () {
        var btn = $(this);
        // Disable btn
        // Enable loader
        $('.btn-loader').addClass('loading');

        setTimeout(function () {
            $.ajax({
                    url: '/poll/jury/manage',
                    data: {
                        'nominated_id': btn.data('nominated-id'),
                    },

                    type: "post",
                    dataType: 'json',
                }
            );
            $(".poll_jury_validate").hide();
            $('.btn-loader').removeClass('loading');
        }, 100);

    })
});

function upload() {
    var imgcanvas = document.getElementById("canv1");
    var fileinput = document.getElementById("finput");
    var image = new SimpleImage(fileinput);
    image.drawTo(imgcanvas);
}


// image registration


// function readURL(input) {
//     if (input.files && input.files[0]) {
//         var reader = new FileReader();
//         reader.onload = function(e) {
//             $('.imagePreview').css('background-image', 'url('+e.target.result +')');
//             $('.imagePreview').hide();
//             $('.imagePreview').fadeIn(650);
//         }
//         reader.readAsDataURL(input.files[0]);
//     }
// }
// $(".imageUpload").change(function() {
//     readURL(this);
// });


/*
$(function() {
  $(".imageUpload").on("change", function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

    if (/^image/.test(files[0].type)) { // only image file
      var reader = new FileReader(); // instance of the FileReader
      reader.readAsDataURL(files[0]); // read the local file

      reader.onloadend = function() { // set image data as background of div
        $(".imagePreview").css("background-image", "url(" + this.result + ")");
      }
    }
  });
  $('.imagePreview').click(function() {
    $('.imageUpload').trigger('click');
  });
});
*/


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


odoo.define('polls_management.custom_list_renderer', function (require) {
    "use strict";
    console.log("Javascript works!");
    var ListRenderer = require('web.ListRenderer');
    var CustomListRenderer = ListRenderer.include({
        events: _.extend({}, ListRenderer.prototype.events, {
            'click thead th.o_column_sortable': '_onSortColumn',
        }),
        _onSortColumn: function (ev) {
            if (this.$el.hasClass('disable_sort')) {
                ev.preventDefault();
                return false;
            }
            return this._super.apply(this, arguments);
        },
    });
    return CustomListRenderer;
});