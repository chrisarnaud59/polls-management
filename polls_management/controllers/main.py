# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import json
import os
from base64 import encode
from datetime import datetime
import pandas as pd

import werkzeug
from werkzeug.urls import url_encode

import odoo

from odoo import http, models, fields, _
from odoo.exceptions import UserError
from odoo.http import request
from werkzeug.datastructures import OrderedMultiDict
from odoo.addons.website.controllers.main import QueryURL
from odoo.osv import expression
from odoo.addons.portal.controllers.web import Home
from odoo.addons.auth_signup.models.res_users import SignupError


class Website(Home):

    @http.route('/contactus', auth="public", csrf=True, website=True)
    def website_form_empty(self, **kwargs):
        ctx = {}
        event = request.env['event.event'].sudo().search([], limit=1)
        partner = request.env.user.partner_id
        company = request.env.company
        countries = request.env['res.country'].sudo().search([])
        continents = request.env['res.partner.continent'].sudo().search([])
        message = "Votre demande a été enregistrée avec succès!"
        values = {
            'continents': continents,
            'countries': countries,
            'post': kwargs
        }
        if kwargs and http.request.httprequest.method == 'POST':
            print("====>>>")
            values.update({
                'message': message,
                'success': 1
            })
            name = kwargs['contact1']
            phone = kwargs['contact2']
            mail = kwargs['contact3']
            message = kwargs['contact5']

            ctx['email_from'] = mail
            ctx['email_to'] = company.email
            ctx['subject'] = kwargs['contact5']
            ctx['send_mail'] = True
            email_values = {
                'email_to': company.email,
                'email_from': kwargs['contact3'],
                'subject': kwargs['contact5'],
                'body_html':
                    f' {message}' \
                    f'<br/> <br/> <br/> <br/> <strong> Nom : </strong>  {name}  <br/> ' \
                    f' Téléphone:  {phone} <br/>' \
                    f'Email: {mail}' \
                }

            mail_template = request.env.ref('polls_management.template_presse_partner').sudo()
            mail_template.send_mail(event.id, force_send=True, email_values=email_values)
        return request.render("website.contactus", values)

    @http.route('/presse', auth="public", csrf=True, website=True, sitemap=True)
    def get_event_presse(self, redirect=None, **post):
        ctx = {}
        event = request.env['event.event'].sudo().search([], limit=1)
        partner = request.env.user.partner_id
        company = request.env.company
        products = request.env['event.product'].sudo().search([], order='amount ASC')
        countries = request.env['res.country'].sudo().search([])
        continents = request.env['res.partner.continent'].sudo().search([])
        message = "Votre demande a été enregistrée avec succès!"
        values = {
            'continents': continents,
            'countries': countries,
            'products': products,
            'post': post
        }
        if post and http.request.httprequest.method == 'POST':
            values.update({
                'message': message,
                'success': 1
            })
            name = post['name']
            phone = post['phone']
            mail = post['email_from']
            message = post['description']

            ctx['email_from'] = mail
            ctx['email_to'] = company.email
            ctx['subject'] = post['description']
            ctx['send_mail'] = True
            email_values = {
                'email_to': company.email,
                'email_from': post['email_from'],
                'subject': post['description'],
                'body_html':
                            f' {message}' \
                            f'<br/> <br/> <br/> <br/> <strong> Nom : </strong>  {name}  <br/> ' \
                            f' Téléphone:  {phone} <br/>' \
                            f'Email: {mail}' \
                }

            mail_template = request.env.ref('polls_management.template_presse_partner').sudo()
            mail_template.send_mail(event.id, force_send=True, email_values=email_values)
        return request.render("polls_management.presse_template", values)

    @http.route('''/events/<model("event.event"):event>/categories''', type='http', auth="public",
                website=True, sitemap=True)
    def get_event_categories(self, event, **kw):
        values = {
            'event': event,
        }
        return request.render("polls_management.home", values)

    @http.route(['/categories'], auth="public", website=True, sitemap=True)
    def index(self, **kw):
        categories = request.env['res.partner.category'].sudo().search([])
        event = request.env['event.event'].sudo().search(
            [('website_published', '=', True), ('date_end', '>=', datetime.now())], limit=1)
        values = {
            'categories': categories.sudo(),
            'event': event.sudo(),
        }

        return request.render("polls_management.all_categories_template", values)

    @http.route('''/<model("event.event"):event>/categories/<model("res.partner.category"):category>/candidats''',
                type='http', auth="public",
                website=True, sitemap=True)
    def get_list_candidate_category(self, event, category, **kw):
        message = 'Votre vote a été enregistré !'
        poller = request.env['res.partner.votant'].sudo().search([('user_id', '=', request.env.user.id)], limit=1)
        partner = request.env['res.partner'].search([('id', '=', request.env.user.partner_id.id)])
        nominates = request.env['res.partner.event.category.score'].sudo().search(
            [('category_id', '=', category.id), ('event_id', '=', event.id)])
        jury = request.env['res.partner.jury'].search([('partner_id', '=', request.env.user.partner_id.id)])
        score_category = request.env['res.partner.event.category.score'].search(
            [('category_id', '=', category.id), ('event_id', '=', event.id)])
        len_poll_jury_id = request.env['res.partner.jury.category.score'].search(
            [('category_id', '=', category.id), ('event_id', '=', event.id), ('number', '=', 1)])
        attendees = []
        for poll_jury in len_poll_jury_id:
            attendees.append(poll_jury.registration_id.id)

        base_url = request.httprequest.url
        score_category.last_url = base_url
        score_category.last_url_all_candidate = base_url
        votants = []
        for rec in category.sudo().res_partner_votant_ids:
            votants.append(rec.sudo().partner_id.id)

        values = {
            'category': category,
            'poller': poller,
            'score_category': score_category,
            'len_poll_jury_id': len_poll_jury_id,
            'votants': votants,
            'partner': partner,
            'voters': nominates,
            'attendees': attendees,
            'event': event,
            'jury': jury,
            'test': request.env.user.partner_id.sudo().id
        }
        if kw:
            values.update({
                'message': message,
                'success': 1,
            })
        return request.render("polls_management.categories_template", values)

    @http.route(
        '''/<model("event.event"):event>/categories/<model("res.partner.category"):category>/candidat/<model("event.registration"):voter>''',
        type='http', auth="public",
        website=True, sitemap=True)
    def get_details_candidates(self, event, category, voter, **kw):
        votants = []
        criterias = []
        partner = request.env['res.partner'].search([('id', '=', request.env.user.partner_id.id)])
        jury = request.env['res.partner.jury'].search([('partner_id', '=', request.env.user.partner_id.id)])
        score_category = request.env['res.partner.event.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)])

        score_category_test = request.env['res.partner.event.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)])

        score_jury_category = request.env['res.partner.jury.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)],
            limit=1)
        percent_score_jury = request.env['res.partner.jury.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)],
            limit=1)

        base_url = request.httprequest.url
        score_category.last_url = base_url
        for rec in category.sudo().res_partner_votant_ids:
            votants.append(rec.sudo().partner_id.id)

        for criteria in category.criteria_ids:
            criterias.append(criteria.id)

        criterias_ids = request.env['res.partner.selection.criteria'].search([])
        riterias = [criteria for criteria in criterias_ids if criteria.id in criterias]

        values = {
            'category': category,
            'event': event,
            'candidate': voter.sudo(),
            'partner': partner,
            'criterias': riterias,
            'jury': jury,
            'score_category_test': score_category_test,
            'percent_score_jury': percent_score_jury,
            'percent_score_jury_zero': 0 if not score_jury_category else '',
            'score_category': score_category,
            # 'score_category': round(score_category.percent_score, 1),
            'score_jury_category': round(score_jury_category.percent_score, 1),
            'test': request.env.user.partner_id.sudo().id,
            'valid': 'oui' if request.env.user.partner_id.sudo().id in votants else 'non',
        }
        return request.render("polls_management.candidate_detail_template", values)

    @http.route(
        '''/<model("event.event"):event>/categories/<model("res.partner.category"):category>/candidats/scores''',
        type='http', auth="public", website=True, sitemap=True)
    def score_candidates_category(self, event, category, **kw):
        nominates = request.env['res.partner.event.category.score'].sudo().search([('category_id', '=', category.id), ('event_id', '=', event.id)], order='user_percent_score DESC')
        len_poll_jury_id = request.env['res.partner.jury.score.event'].sudo().search([('category_id', '=', category.id), ('event_id', '=', event.id)])
        votants = []
        attendees = []
       # score_jury_category = request.env['res.partner.jury.category.score'].search([('category_id', '=', category.id), ('event_id', '=', event.id)], limit=1)
        for rec in category.sudo().res_partner_votant_ids:
            votants.append(rec.sudo().partner_id.id)
        for poll_jury in len_poll_jury_id:
            attendees.append(poll_jury.registration_id.id)

        values = {
            'category': category,
            'voters': nominates,
            'attendees': attendees,
            'len_poll_jury_id': len_poll_jury_id,
            'event': event,
        }
        return request.render("polls_management.score_category_template", values)


    @http.route(
        '''/<model("event.event"):event>/categories/<model("res.partner.category"):category>/candidat/<model("event.registration"):voter>/jury/scores''',
        type='http', csrf=False, auth="public", website=True, sitemap=True)
    def get_poll_jury(self, event, category, voter, **kw):
        print("====>word %s", event)
        votants = []
        criterias = []
        message = 'Vote enregistré'
        partner = request.env['res.partner'].search([('id', '=', request.env.user.partner_id.id)])
        jury = request.env['res.partner.jury'].search([('partner_id', '=', request.env.user.partner_id.id)])
        score_jury = request.env['res.partner.jury.category.score'].search([])
        score_category = request.env['res.partner.event.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)])
        len_poll_jury_id = request.env['res.partner.jury.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id),
             ('jury_id', '=', jury.id)])

        score_category__ = request.env['res.partner.event.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)])

        score_category_test = request.env['res.partner.event.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)])

        score_jury_category = request.env['res.partner.jury.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)],
            limit=1)
        percent_score_jury = request.env['res.partner.jury.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)],
            limit=1)


        base_url = request.httprequest.url
        score_category.last_url = base_url
        criteria_poll = request.httprequest.form.getlist('criteria_poll')
        for rec in category.sudo().res_partner_votant_ids:
            votants.append(rec.sudo().partner_id.id)

        for criteria in category.criteria_ids:
            criterias.append(criteria.id)

        criterias_ids = request.env['res.partner.selection.criteria'].search([], order='id ASC')
        riterias = [criteria for criteria in criterias_ids if criteria.id in criterias]
        test = []
        if kw and http.request.httprequest.method == 'POST':
            print("===>==WR %s", kw)

            for poll in criteria_poll:
                test.append(int(poll))
            essai = zip(riterias, test)
            my_dict = dict(essai)
            n = 0
            for rit in riterias:
                n += 1
                score = int(''.join([str(my_dict.get(index)) for index in my_dict if index.id == rit.id]))
                score_jury_val = score_jury.create({
                    'event_id': event.id,
                    'category_id': category.id,
                    'registration_id': voter.id,
                    'score': score,
                    # 'score_total': score,
                    'jury_id': jury.id,
                    'criteria_id': rit.id,
                    'number': n
                })
            scores = []
            len_poll_jury = request.env['res.partner.jury.category.score'].search(
                [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)])
            testons = request.env['res.partner.jury.score.event']
            testons.create({
                'jury_id': jury.id,
                'event_id': event.id,
                'category_id': category.id,
                'registration_id': voter.id,
                'score': sum(len_poll_jury.mapped('score')),
                'score_total': sum(len_poll_jury.mapped('score')) / len(category.criteria_ids),
            })
            for score in len_poll_jury:
                scores.append(score.score)
            score_category.score += sum(scores)
            score_category.compute_percent_score()
            for n in range(sum(scores)):
                values = {
                    'name': partner.name if partner else '',
                    'partner_id': partner.id if partner else '',
                    'user_id': partner.user_id.id if partner else '',
                    'score_category': 1,
                    'category_id': category.id,
                    'registration_id': voter.id,
                    'event_id': event.id
                }
                category.res_partner_votant_ids = [(0, 0, values)]
            score_total = request.env['res.partner.votant'].search(
                [('event_id', '=', event.id), ('category_id', '=', category.id)])
            for score in request.env['res.partner.event.category.score'].search(
                    [('event_id', '=', event.id), ('category_id', '=', category.id)]):
                score.score_total = len(score_total)
                score.compute_percent_score()
            total_jury_score = []
            for score_jury in request.env['res.partner.jury.category.score'].search(
                    [('event_id', '=', event.id), ('category_id', '=', category.id)]):
                total_jury_score.append(score_jury.score)
                score_jury.score_total = sum(total_jury_score)
                score_jury.compute_percent_jury_score()

            return request.redirect('/')
        values = {
            'score_jury_criteria': request.env['res.company'].search([], limit=1).score_jury,
            'category': category,
            'event': event,
            'candidate': voter.sudo(),
            'partner': partner,
            'criterias': riterias,
            'jury': jury,
            'score_category_test': score_category_test,
            'percent_score_jury': percent_score_jury,
            'percent_score_jury_zero': 0 if not score_jury_category else '',
            'score_category': score_category,
            'score_jury_category': round(score_jury_category.percent_score, 1),
            'message': message if kw else '',
            'len_poll_jury_id': len_poll_jury_id,
            'score_category': round(score_category.percent_score, 1),
            'test': request.env.user.partner_id.sudo().id,
            'valid': 'oui' if request.env.user.partner_id.sudo().id in votants else 'non',
        }
        return request.render("polls_management.res_partner_jury_score_template", values)

    @http.route(
        [
            '/<model("event.event"):event>/categories/<model("res.partner.category"):category>/candidat/<model("event.registration"):voter>/jury/scores'],
        type='http', csrf=False, auth="public", website=True, sitemap=True)
    def get_view_poll_jury(self, event, category, voter, **kw):
        votants = []
        criterias = []
        partner = request.env['res.partner'].search([('id', '=', request.env.user.partner_id.id)])
        jury = request.env['res.partner.jury'].search([('partner_id', '=', request.env.user.partner_id.id)])
        score_jury = request.env['res.partner.jury.category.score'].search(
            [('registration_id', '=', voter.id), ('jury_id', '=', jury.id), ('category_id', '=', category.id),
             ('event_id', '=', event.id)])
        score_category = request.env['res.partner.event.category.score'].search(
            [('registration_id', '=', voter.id), ('category_id', '=', category.id), ('event_id', '=', event.id)])
        base_url = request.httprequest.url
        score_category.last_url = base_url
        values = {
            'category': category,
            'event': event,
            'candidate': voter.sudo(),
            'partner': partner,
            'jury': jury,
            'score_jury': score_jury,
            'score_category': round(score_category.percent_score, 1),
            'test': request.env.user.partner_id.sudo().id,
            'valid': 'oui' if request.env.user.partner_id.sudo().id in votants else 'non',
        }
        return request.render("polls_management.res_partner_view_jury_score_template", values)

    @http.route('''/poll/manage''', auth="user", csrf=False)
    def update_polls(self, **post):
        candidate = request.env['event.registration'].sudo().search([('id', '=', int(post['candidate_id']))])
        category = request.env['res.partner.category'].sudo().search([('id', '=', int(post['category_id']))])
        partner = request.env['res.partner'].search([('id', '=', request.env.user.partner_id.id)])
        event_active = request.env['event.event'].sudo().search([('id', '=', int(post['event_id']))])
        score_category = request.env['res.partner.event.category.score'].search(
            [('registration_id', '=', candidate.id), ('category_id', '=', category.id),
             ('event_id', '=', event_active.id)])
        message = "Votre vote a été enregistré avec succès!"
        if post:
            if not request.session.uid:
                return request.redirect('/web/login')
            else:
                if candidate:
                    values = {
                        'name': partner.name if partner else '',
                        'partner_id': partner.id if partner else '',
                        'user_id': partner.user_id.id if partner else '',
                        'score_category': 1,
                        'category_id': category.id,
                        'registration_id': candidate.id,
                        'event_id': event_active.id,
                    }

                    category.res_partner_votant_ids = [(0, 0, values)]
                    score_total = request.env['res.partner.votant'].search(
                        [('event_id', '=', event_active.id), ('category_id', '=', category.id)])
                    if score_category:
                        score_category.score += 1
                        score_category.compute_percent_score()
                        for score in request.env['res.partner.event.category.score'].search([]):
                            score.compute_percent_score()
                            score.score_total = len(score_total)
                    else:
                        request.env['res.partner.event.category.score'].sudo().create({
                            'event_id': event_active.id,
                            'category_id': category.id,
                            'registration_id': candidate.id,
                            'score': 1
                        })

    @http.route('''/poll/details/manage''', auth="user", csrf=False)
    def update_polls_details(self, **post):
        nominate = request.env['event.registration'].sudo().search([('id', '=', int(post['nominated_id']))])
        category = request.env['res.partner.category'].sudo().search([('id', '=', int(post['category_id']))])
        partner = request.env['res.partner'].search([('id', '=', request.env.user.partner_id.id)])
        event_active = request.env['event.event'].sudo().search([('id', '=', int(post['event_id']))])
        score_category = request.env['res.partner.event.category.score'].search(
            [('registration_id', '=', nominate.id), ('category_id', '=', category.id),
             ('event_id', '=', event_active.id)])
        if post:
            if not request.session.uid:
                return request.redirect('/web/login')
            else:
                if nominate:
                    # candidate.score += 1
                    values = {
                        'name': partner.name if partner else '',
                        'partner_id': partner.id if partner else '',
                        'user_id': partner.user_id.id if partner else '',
                        'score_category': 1,
                        'category_id': category.id,
                        'registration_id': nominate.id,
                        'event_id': event_active.id
                    }

                    category.res_partner_votant_ids = [(0, 0, values)]
                    score_total = request.env['res.partner.votant'].search(
                        [('event_id', '=', event_active.id), ('category_id', '=', category.id)])
                    if score_category:
                        score_category.score += 1
                        score_category.compute_percent_score()
                        for score in request.env['res.partner.event.category.score'].search([]):
                            score.compute_percent_score()
                            score.score_total = len(score_total)
                    else:
                        request.env['res.partner.event.category.score'].sudo().create({
                            'event_id': event_active.id,
                            'category_id': category.id,
                            'registration_id': nominate.id,
                            'score': 1
                        })

    @http.route('/web/download/binary/<string:model>/<int:res_id>/<string:field>/<string:filename>',
                type='http', auth="public")
    def download_binary_document(self, model, field, res_id, filename=None, **kw):
        """
        Download file from portal
        :param model:
        :param field:
        :param res_id:
        :param filename:
        :param kw:
        :return:
        """

        status, headers, content = request.env['ir.http'].sudo().binary_content(
            model=model,
            id=res_id,
            field=field,
            download=True,
            default_mimetype='application/octet-stream',
        )

        if status != 200:
            return request.env['ir.http'].sudo()._response_by_status(status, headers, content)
        else:
            content_base64 = base64.b64decode(content)
            headers.append(('Content-Length', len(content_base64)))
            response = request.make_response(content_base64, headers)

        return response


class WebsiteEventController(http.Controller):

    def sitemap_event(env, rule, qs):
        if not qs or qs.lower() in '/events':
            yield {'loc': '/events'}

    # ------------------------------------------------------------
    # EVENT LIST
    # ------------------------------------------------------------

    def _get_events_search_options(self, **post):
        return {
            'displayDescription': False,
            'displayDetail': False,
            'displayExtraDetail': False,
            'displayExtraLink': False,
            'displayImage': False,
            'allowFuzzy': not post.get('noFuzzy'),
            'date': post.get('date'),
            'tags': post.get('tags'),
            'type': post.get('type'),
            'country': post.get('country'),
        }

    @http.route(['/', '/event', '/event/page/<int:page>', '/events', '/events/page/<int:page>'], type='http',
                auth="public",
                website=True, sitemap=sitemap_event)
    def events(self, page=1, **searches):
        Event = request.env['event.event']
        SudoEventType = request.env['event.type'].sudo()

        searches.setdefault('search', '')
        searches.setdefault('date', 'upcoming')
        searches.setdefault('tags', '')
        searches.setdefault('type', 'all')
        searches.setdefault('country', 'all')

        website = request.website

        step = 12  # Number of events per page

        options = self._get_events_search_options(**searches)
        order = 'date_begin'
        if searches.get('date', 'upcoming') == 'old':
            order = 'date_begin desc'
        order = 'is_published desc, ' + order
        search = searches.get('search')
        event_count, details, fuzzy_search_term = website._search_with_fuzzy("events", search,
                                                                             limit=page * step, order=order,
                                                                             options=options)
        event_details = details[0]
        events = event_details.get('results', Event)
        events = events[(page - 1) * step:page * step]

        # count by domains without self search
        domain_search = [('name', 'ilike', fuzzy_search_term or searches['search'])] if searches['search'] else []

        no_date_domain = event_details['no_date_domain']
        dates = event_details['dates']
        for date in dates:
            if date[0] not in ['all', 'old']:
                date[3] = Event.search_count(expression.AND(no_date_domain) + domain_search + date[2])

        no_country_domain = event_details['no_country_domain']
        countries = Event.read_group(expression.AND(no_country_domain) + domain_search, ["id", "country_id"],
                                     groupby="country_id", orderby="country_id")
        countries.insert(0, {
            'country_id_count': sum([int(country['country_id_count']) for country in countries]),
            'country_id': ("all", _("All Countries"))
        })

        search_tags = event_details['search_tags']
        current_date = event_details['current_date']
        current_type = None
        current_country = None

        if searches["type"] != 'all':
            current_type = SudoEventType.browse(int(searches['type']))

        if searches["country"] != 'all' and searches["country"] != 'online':
            current_country = request.env['res.country'].browse(int(searches['country']))

        pager = website.pager(
            url="/event",
            url_args=searches,
            total=event_count,
            page=page,
            step=step,
            scope=5)

        keep = QueryURL('/event', **{
            key: value for key, value in searches.items() if (
                    key == 'search' or
                    (value != 'upcoming' if key == 'date' else value != 'all'))
        })

        searches['search'] = fuzzy_search_term or search
        event_actual = request.env['event.event'].search([], limit=1)

        values = {
            'current_date': current_date,
            'current_country': current_country,
            'current_type': current_type,
            'event_ids': events,  # event_ids used in website_event_track so we keep name as it is
            'dates': dates,
            'event_actual': event_actual,
            'categories': request.env['event.tag.category'].search([
                ('is_published', '=', True), '|', ('website_id', '=', website.id), ('website_id', '=', False)
            ]),
            'countries': countries,
            'pager': pager,
            'searches': searches,
            'search_tags': search_tags,
            'keep': keep,
            'search_count': event_count,
            'original_search': fuzzy_search_term and search,
            'website': website
        }

        if searches['date'] == 'old':
            # the only way to display this content is to set date=old so it must be canonical
            values['canonical_params'] = OrderedMultiDict([('date', 'old')])

        return request.render("website_event.index", values)

    def _process_tickets_form(self, event, form_details):
        """ Process posted data about ticket order. Generic ticket are supported
        for event without tickets (generic registration).

        :return: list of order per ticket: [{
            'id': if of ticket if any (0 if no ticket),
            'ticket': browse record of ticket if any (None if no ticket),
            'name': ticket name (or generic 'Registration' name if no ticket),
            'quantity': number of registrations for that ticket,
        }, {...}]
        """
        ticket_order = {}
        for key, value in form_details.items():
            registration_items = key.split('nb_register-')
            if len(registration_items) != 2:
                continue
            ticket_order[int(registration_items[1])] = int(value)

        ticket_dict = dict((ticket.id, ticket) for ticket in request.env['event.event.ticket'].sudo().search([
            ('id', 'in', [tid for tid in ticket_order.keys() if tid]),
            ('event_id', '=', event.id)
        ]))

        return [{
            'id': tid if ticket_dict.get(tid) else 0,
            'ticket': ticket_dict.get(tid),
            'name': ticket_dict[tid]['name'] if ticket_dict.get(tid) else _('Registration'),
            'quantity': count,
        } for tid, count in ticket_order.items() if count]

    @http.route(['/event/<model("event.event"):event>/registration/new'], type='json', auth="public", methods=['POST'],
                website=True)
    def registration_new(self, event, **post):
        countries = request.env['res.country'].sudo().search([])
        categories = request.env['res.partner.category'].sudo().search([])
        continents = request.env['res.partner.continent'].sudo().search([])
        tickets = self._process_tickets_form(event, post)
        availability_check = True
        if event.seats_limited:
            ordered_seats = 0
            for ticket in tickets:
                ordered_seats += ticket['quantity']
            if event.seats_available < ordered_seats:
                availability_check = False
        if not tickets:
            return False
        default_first_attendee = {}
        if not request.env.user._is_public():
            default_first_attendee = {
                "name": request.env.user.name,
                "email": request.env.user.email,
                "phone": request.env.user.mobile or request.env.user.phone,
                "community_id": 'test'
            }
        else:
            visitor = request.env['website.visitor']._get_visitor_from_request()
            if visitor.email:
                default_first_attendee = {
                    "name": visitor.display_name,
                    "email": visitor.email,
                    "phone": visitor.mobile,
                    "community_id": 'test'
                }
        return request.env['ir.ui.view']._render_template("website_event.registration_attendee_details", {
            'tickets': tickets,
            'event': event,
            'availability_check': availability_check,
            'default_first_attendee': default_first_attendee,
            'countries': countries,
            'categories': categories,
            'continents': continents,
        })

    @http.route(['''/event/<model("event.event"):event>/registration/confirm'''], type='http', auth="public",
                methods=['POST'], website=True)
    def registration_confirm(self, event, **post):
        Attachments = request.env['ir.attachment']
        partner = request.env['res.partner'].search([('id', '=', request.env.user.partner_id.id)])
        registration = request.env['event.registration']
        category_ids = request.httprequest.form.getlist('category_ids')
        files = request.httprequest.files.getlist('image')
        continent = request.env['res.partner.continent'].search([('code', '=', str(post.get('continent')))])
        if post:
            image = post.get('imageUpload')
            values = {
                'name': str(post.get('name')) + ' ' + str(post.get('lastname')),
                'email': str(post.get('email')),
                'community': str(post.get('community')),
                'birth_date': str(post.get('birth_date')),
                'function': str(post.get('function')),
                'street1': str(post.get('street1')),
                'street2': str(post.get('street2')),
                'baptism_date': str(post.get('baptism_date')),
                'community_leader': str(post.get('community_leader')),
                'contact_to_urgence': str(post.get('contact_to_urgence')),
                'number_to_urgence_contact': post.get('number_to_urgence_contact'),
                'create_date': datetime.now(),
                'event_id': event.id,
                'continent_id': continent.id,
                'image_file': base64.encodebytes(image.read()),
                'gender': 'male' if str(post.get('gender')) == 'Homme' else 'female',
                'partner_id': partner.id if partner else '',
                'country_id': request.env['res.country'].search([('id', '=', int(post.get('country_id')))]).id,
                'score_event_category_ids': [(0, 0,
                                              {
                                                  'event_id': event.id,
                                                  'category_id': int(category),
                                                  'score': 0
                                              }) for category in category_ids]
            }
            registration.create(values)
            attachment_id = Attachments.sudo().create({
                'name': image.filename,
                'res_name': image.filename,
                'type': 'binary',
                'res_model': 'event.registration',
                'res_id': registration.id,
                'datas': base64.b64encode(image.read()),
            })

        return request.redirect('/event')


class HomeExtended(Home):

    def _login_redirect(self, uid, redirect=None):
        """
        Override this function for redirect login portal user to account view
        :param uid:
        :param redirect:
        :return:
        """
        score_event = request.env['res.partner.event.category.score'].search([], limit=1)
        if not redirect and not request.env['res.users'].sudo().browse(uid).has_group('base.group_user'):
            redirect = '/categories'
        else:
            redirect = '/categories'
        return super(HomeExtended, self)._login_redirect(uid, redirect=redirect)

