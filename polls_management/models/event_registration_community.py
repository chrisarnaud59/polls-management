from odoo import fields, models, api


class EventRegistrationCommunity(models.Model):
    _name = 'event.registration.community'
    _description = 'event.registration.community'

    name = fields.Char('Nom', required=True)
    description = fields.Text('Description')