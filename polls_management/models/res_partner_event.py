from odoo import fields, models, api
from random import randint


class EventRegistration(models.Model):
    _inherit = 'event.registration'

    image_file = fields.Binary("Image")
    description = fields.Text("Description")
    score_event_category_ids = fields.One2many('res.partner.event.category.score', 'registration_id', string='Scores')
    score_jury_category_ids = fields.One2many('res.partner.jury.category.score', 'registration_id',
                                              string='Scores Jury')
    video_link = fields.Char("Lien vidéo de présentation", help="Récuperer le code d'intégration d'une vidéo youtube")
    community_id = fields.Many2one('event.registration.community', string="Communauté")
    community = fields.Char("Communauté")
    community_leader = fields.Char("Leader de communauté")
    contact_to_urgence = fields.Char("Contact d'urgence")
    number_to_urgence_contact = fields.Char("Numéro contact d'urgence")
    birth_date = fields.Date(string="Date de naissance")
    baptism_date = fields.Date(string="Date de baptême")
    function = fields.Char(string="Fonction au sein de l'Église")
    country_id = fields.Many2one('res.country', string='Pays', default=lambda self: self.env['res.country'].search([('code', '=', 'CI')]))
    street1 = fields.Char('Commune')
    street2 = fields.Char('Quartier')
    gender = fields.Selection([('male', 'Homme'), ('female', 'Femme')], string='Sexe')
    continent_id = fields.Many2one('res.partner.continent', string='Continent', default=lambda self: self.env['res.partner.continent'].search([('code', '=', 'afrik')]))

    def _get_website_registration_allowed_fields(self):
        res = super()._get_website_registration_allowed_fields()
        res.add('community_id')
        return res
        # return {'name', 'phone', 'email', 'company_name', 'event_id', 'partner_id', 'event_ticket_id', 'community_id'}


class ResPartnerContinent(models.Model):
    _name = 'res.partner.continent'
    _description = 'res.partner.continent'

    name = fields.Char('Continent')
    code = fields.Char('code')
