from odoo import fields, models, api


class ResPartnerWinnerPrize(models.Model):
    _name = "res.partner.winner.prize"
    _description = "res.partner.winner.prize"

    name = fields.Char('Prix')