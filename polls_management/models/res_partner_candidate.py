from odoo import fields, models, api


class ResPartnerCandidate(models.Model):
    _name = "res.partner.candidate"
    _description = "Candidate"
    _inherits = {'res.partner': 'partner_id'}
    _inherit = ['mail.thread', 'mail.activity.mixin']

    partner_id = fields.Many2one('res.partner')
    score = fields.Integer('Score Total')
    gender = fields.Selection([('male', 'Homme'), ('female', 'Femme'), ('other', 'Autre')], default='male', string="Sexe")
    birth_date = fields.Date(string="Date de naissance")
    description = fields.Text('Description')
    #score_event_category_ids = fields.One2many('res.partner.event.category.score', 'candidate_id', string='Scores')


