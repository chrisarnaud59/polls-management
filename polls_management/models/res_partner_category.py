from odoo import fields, models, api


class ResPartnerCategory(models.Model):
    _inherit = "res.partner.category"

    company_id = fields.Many2one('res.company', string='Compagnie', default=lambda self: self.env.company)
    voter_ids = fields.Many2many('event.registration', string='Nominés')
    res_votant_ids = fields.Many2many('res.users')
    res_partner_votant_ids = fields.Many2many('res.partner.votant')
    criteria_ids = fields.Many2many('res.partner.selection.criteria', string='Crtières')
    event_ids = fields.Many2many('event.event',  string='Évènements')
    score_event_category_ids = fields.One2many('res.partner.event.category.score', 'category_id', string='Scores')
    res_partner_jury_ids = fields.Many2many('res.partner.jury', string='Membres du jury')
    winner_prize_ids = fields.Many2many('res.partner.winner.prize', string='Prix')
    score_total_jury_category = fields.Integer(string="Score Total du jury", compute="compute_score_total_jury_category", store=True)
    block_polling = fields.Boolean(string="Impossible de voter", default=True)
    result_proclamed = fields.Boolean(string="Résultat proclamés", default=False)

    @api.depends('criteria_ids')
    @api.onchange('criteria_ids')
    def compute_score_total_jury_category(self):
        self.score_total_jury_category = len(self.criteria_ids) * self.company_id.score_jury
