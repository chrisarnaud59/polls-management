# -*- coding: utf-8 -*-
from odoo import fields, models, api
from datetime import datetime


class EventStage(models.Model):
    _inherit = 'event.stage'

    active = fields.Boolean('Archivé', default=True)