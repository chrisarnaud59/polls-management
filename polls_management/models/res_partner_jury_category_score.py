from odoo import fields, models, api, _, tools


class ResPartnerJuryCategoryScore(models.Model):
    _name = 'res.partner.jury.category.score'
    _description = 'res.partner.jury.category.score'

    jury_id = fields.Many2one('res.partner.jury', string='Jury')
    event_id = fields.Many2one('event.event', string='Évènement')
    category_id = fields.Many2one('res.partner.category', string='Catégorie')
    registration_id = fields.Many2one('event.registration', string='Candidat')
    criteria_id = fields.Many2one('res.partner.selection.criteria', string='Critères')
    company_id = fields.Many2one('res.company', string='Compagnie')
    score = fields.Integer('Score')
    score_total = fields.Integer('Score Total')
    score_totals = fields.Float('Score Total')
    score_total_all_criterias = fields.Integer('Score tous critères')
    score_total_all = fields.Float('Score tous critères')
    percent_score = fields.Float('Pourcentage', compute="compute_percent_jury_score", store=True)
    number = fields.Integer()

    @api.depends('score')
    @api.onchange('score')
    def compute_percent_jury_score(self):
        judges = []
        jurys = self.env['res.partner.jury'].search([])
        for jury in jurys:
            judges.append(jury.partner_id.id)
        for rec in self:
            votants = self.env['res.partner.votant'].search([('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id), ('partner_id', 'in', judges)], limit=1)
            votant_id = self.env['res.partner.votant'].search([('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id), ('partner_id', 'in', judges), ('registration_id', '=', rec.registration_id.id)])
            n_criteria = self.env['res.partner.votant'].search([('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id), ('partner_id', 'in', judges), ('registration_id', '=', rec.registration_id.id)], limit=1)
            jury_score = self.env['res.partner.jury.score.event'].search([('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id), ('registration_id', '=', rec.registration_id.id)])

            #rec.score_total =
            rec.score_total = len(votant_id)
            rec.score_totals = len(votant_id)
            rec.score_total_all = sum(jury_score.mapped('score_total'))
            #rec.score_total = len(votants)
            rec.percent_score = (len(votant_id) / rec.score_total)*100


class ResPartnerJuryScoreEvent(models.Model):
    _name = 'res.partner.jury.score.event'
    _description = 'res.partner.jury.score.event'

    jury_id = fields.Many2one('res.partner.jury', string='Jury')
    event_id = fields.Many2one('event.event', string='Évènement')
    category_id = fields.Many2one('res.partner.category', string='Catégorie')
    registration_id = fields.Many2one('event.registration', string='Candidat')
    score = fields.Float('Score')
    score_total = fields.Float('Score')
    percent_score = fields.Float('Pourcentage', compute="compute_percent_jury_score", store=True)








