from odoo import fields, models, api, _
from odoo.exceptions import ValidationError


class ResPartnerSelectionCriteria(models.Model):
    _name = "res.partner.selection.criteria"
    _description = "res.partner.selection.criteria"

    name = fields.Char(string='Éthique')
    score_max = fields.Integer('Note max')
    detail_ids = fields.Many2many('res.partner.criteria.details')

    @api.constrains('score_max')
    def _constraint_score_max(self):
        for rec in self:
            if self.env.company.score_jury and rec.score_max > self.env.company.score_jury:
                raise ValidationError(_('Note du critère ne doit pas dépasser la note maximale (%s) paramétrée', self.env.company.score_jury))


class ResPartnerSelectionCriteriaDetails(models.Model):
    _name = "res.partner.criteria.details"
    _description = "res.partner.criteria.details"

    name = fields.Char('Détail')
    description = fields.Text('Description')

