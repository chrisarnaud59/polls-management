from odoo import fields, models, api


class ResPartnerVotant(models.Model):
    _name = "res.partner.votant"
    _description = "res.partner.votant"
    _inherits = {'res.partner': 'partner_id'}
    _inherit = ['mail.thread', 'mail.activity.mixin']

    partner_id = fields.Many2one('res.partner', string='Partner')
    user_id = fields.Many2one('res.users', string='User')
    event_id = fields.Many2one('event.event', string="Évènements")
    score_category = fields.Integer('Score', default=0)
    name = fields.Char(string='Nom')
    category_id = fields.Many2one('res.partner.category', string='Catégories')
    registration_id = fields.Many2one('event.registration', string='Nominé')