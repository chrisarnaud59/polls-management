from odoo import fields, models, api


class ResPartnerJury(models.Model):
    _name = "res.partner.jury"
    _description = "res.partner.jury"
    _inherits = {'res.partner': 'partner_id'}
    _inherit = ['mail.thread', 'mail.activity.mixin']

    partner_id = fields.Many2one('res.partner', string='Partner')
    user_id = fields.Many2one('res.users', string='User')
    event_id = fields.Many2one('event.event', string="Évènements")
    category_id = fields.Many2one('res.partner.category', string='Catégories')