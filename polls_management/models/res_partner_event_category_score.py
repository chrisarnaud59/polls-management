from odoo import fields, models, api, _, tools


class ResPartnerEventCategoryScore(models.Model):
    _name = 'res.partner.event.category.score'
    _description = 'res.partner.event.category.score'
    _order = 'totals_score DESC'

    event_id = fields.Many2one('event.event', string='Évènement')
    category_id = fields.Many2one('res.partner.category', string='Catégorie')
    registration_id = fields.Many2one('event.registration', string='Candidat')
    description = fields.Text('Description', related='registration_id.description')
    photo = fields.Binary('Photo', related='registration_id.image_file')
    number = fields.Char('Ordre', compute='_compute_number')
    score = fields.Integer('Score')
    score_total = fields.Integer('Score Total', store=True)
    totals_public = fields.Float('Score Total Public', compute="compute_percent_score", store=True)
    totals_jury = fields.Float('Score Total Jury', compute="compute_percent_score", store=True)
    total_score_total = fields.Integer('Score Total', compute="compute_percent_score", store=True)
    totals_score = fields.Float('Score Total', compute="compute_percent_score", store=True)
    percent_public = fields.Float('Score public (%)', compute="compute_percent_score", store=True)
    percent_jury = fields.Float('Score jury (%)', compute="compute_percent_score", store=True)
    percent_score = fields.Float('Pourcentage', compute="compute_percent_score", store=True)
    user_percent_score = fields.Float('Pourcentage public', compute="compute_user_percent_score", store=True)
    last_url = fields.Char(string="Last URL")
    last_url_all_candidate = fields.Char(string="Last URL")

    @api.depends
    def _compute_photo(self):
        for rec in self:
            rec.photo = rec.registration_id.image_file

    def _compute_number(self):
        n = 0
        for rec in self:
            n+=1
            rec.number = str(n)+'er' if n==1 else str(n)+'e'

    @api.depends('score', 'score_total', 'totals_public', 'totals_jury')
    @api.onchange('score', 'score_total', 'totals_public', 'totals_jury')
    def compute_percent_score(self):
        judges = []
        jurys = self.env['res.partner.jury'].search([])
        for jury in jurys:
            judges.append(jury.partner_id.id)
        for rec in self:
            votants = self.env['res.partner.votant'].search([('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id)])
            publics = self.env['res.partner.votant'].search([('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id), ('partner_id', 'not in', judges)])
            jury_poll = self.env['res.partner.jury.score.event'].search([('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id), ('registration_id', '=', rec.registration_id.id)])
            public = self.env['res.partner.votant'].search([('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id), ('partner_id', 'not in', judges), ('registration_id', '=', rec.registration_id.id)])


            event = self.env['event.event'].search([], limit=1)
            rec.totals_public = len(public)
            rec.percent_public = (len(public) * event.percent_public)/100

            rec.totals_jury = sum(jury_poll.mapped('score_total'))
            rec.percent_jury = (sum(jury_poll.mapped('score_total')) * event.percent_jury)/100

            rec.totals_score = (rec.totals_public * event.percent_public)/100 + (rec.totals_jury * event.percent_jury)/100

            if rec.score_total>0:
                rec.percent_score = (rec.score/rec.score_total)*100
                #rec.user_percent_score = (len(public)/len(publics))*100
            else:
                rec.percent_score = 0
                rec.user_percent_score = 0

    @api.depends('score', 'score_total')
    @api.onchange('score', 'score_total')
    def compute_user_percent_score(self):
        judges = []
        jurys = self.env['res.partner.jury'].search([])
        for jury in jurys:
            judges.append(jury.partner_id.id)
        for rec in self:
            publics = self.env['res.partner.votant'].search(
                [('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id),
                 ('partner_id', 'not in', judges)])
            public = self.env['res.partner.votant'].search(
                [('event_id', '=', rec.event_id.id), ('category_id', '=', rec.category_id.id),
                 ('partner_id', 'not in', judges), ('registration_id', '=', rec.registration_id.id)])

            if publics:
                rec.user_percent_score = (len(public)/len(publics))*100
            else:
                rec.user_percent_score = 0


