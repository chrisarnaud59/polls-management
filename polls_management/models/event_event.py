from odoo import fields, models, api
from random import randint


class Event(models.Model):
    _inherit = 'event.event'

    def _get_default_color(self):
        return randint(1, 11)

    color = fields.Integer(string='Couleur', default=_get_default_color)
    percent_public = fields.Float(string="Pourcentage public", store=True)
    percent_jury = fields.Float(string="Pourcentage jury", store=True)
    registration_id = fields.Many2one('event.registration', string="Nominé")
    high_patronage_id = fields.Many2one('res.partner', string="Haut patronage")
    patronage_id = fields.Many2one('res.partner', string="Patronage")
    co_chair_ids = fields.Many2many('res.partner', 'co_chair_ids_rel', string="Co-Parrainages")
    ambassador_ids = fields.Many2many('res.partner', 'ambassador_ids_rel', string="Ambassadeurs")
    expected_partner_ids = fields.Many2many('res.partner', 'expected_partner_ids_rel', string="Partenaires atendus")
    category_ids = fields.Many2many('res.partner.category', string="Catégories")
    winner_of_the_year_award_ids = fields.Many2many('res.partner.winner.prize', string="Lauréats de prix")
